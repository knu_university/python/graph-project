import networkx as nx
import matplotlib.pyplot as plt
"""
Parametrs of graph:

The number of vertices is 11,
Rib size - 10,
There are 4 components of SV.
"""


def create_graph():
    g = nx.path_graph(2)
    nx.add_cycle(g, [2, 3, 4])
    nx.add_cycle(g, [5, 6, 7])
    nx.add_cycle(g, [8, 9, 10,11,12])
    g.add_edge(8,10)
    return g


def draw_graph(g, positions, params):
    nx.draw(g, pos=positions, **params)
    plt.show()


def save_graph(g, path):
    nx.write_adjlist(g, path)

def read_graph(path):
    return nx.read_adjlist(path, nodetype=int)

def position(numberofNodes):  
    pos_y = -1
    pos = {}
    for i in range(numberofNodes):
        if(pos_y == 2):
            pos_y = -1
        pos[i] = (i, pos_y)
        pos_y += 1
    return pos

def connectedComponentsInfo(g: nx.graph, number):
    """
        Programmatically bypass the connectivity components of the graph. 
        For each in UNDERSTANDABLE look in the text mode:
         the number of vertices and edges, powers and eccentricities of vertices, radius and diameter
    """
    info = f"Component {number+1}: \n"
    info += "|V|="+str(len(g.nodes)) + "\n"
    info += "|E|="+str(len(g.edges()))+"\n"
    info += "R="+str(nx.radius(g))+"\n"
    info += "D="+str(nx.diameter(g))+"\n"
    for key, value in nx.eccentricity(g).items():
        info += f"e({key}) = {value}" + "  "
    info += "\n"
    for key, value in g.degree:
        info += f"v({key}) = {value}" + "  "
    return info + "\n"

def showDiametr(g: nx.graph, cc, params):
    """
    For each non-trivial component of the connection, programmatically find at least one diameter.
    Construct a graphic image where the edges and vertices included in the found parameters are colored.
    """
    diametr = []
    diametr_nodes=[]
    for subGraph in cc:  # devide on CC
        eccentr = nx.eccentricity(subGraph)
        max_eccentr_value = max(eccentr.values())
        start = next(iter(key for key, value in eccentr.items() if value == max_eccentr_value))  
        path = nx.single_source_shortest_path(subGraph, start) 
        for i in next(iter(el for el in path.values() if len(el)-1 == max_eccentr_value)): # return only first value
            diametr_nodes.append(i)   
    
    params['node_color'] = ['red' if (v in diametr_nodes) else 'white' for v in g.nodes()]
    params['edge_color'] = ['blue' if (v in diametr_nodes) and (w in diametr_nodes) else 'black' for v, w in g.edges]


def forest(cc: list, g, params):
    """
    Programmatically build a deep bone loess of the graph.
    Construct an image of the graph on which the edges of the found fox are colored.
    """
    frst = []
    for el in cc:
        for i in list(nx.dfs_edges(el)):
            frst.append(i)
    params['style'] = ['--' if (el in frst) else 'solid' for el in list(g.edges)]
    params['edge_color'] = ['green' if (el in frst) else 'black' for el in list(g.edges)]


def main():
    plt.axes().set_aspect('equal', adjustable='datalim')
    DEFAULT_PARAMS = {'with_labels': True, 'font_weight': 'bold',
                      'edgecolors': 'black', 'font_color': 'black', 'node_color': 'white'}
    
    params = DEFAULT_PARAMS
    path = "graph.txt"

    g = create_graph()
    cc = [g.subgraph(c) for c in nx.connected_components(g)]
    save_graph(g, path)
    g = read_graph(path)
    for i in range(len(cc)):
        print(connectedComponentsInfo(cc[i], i))
     

    showDiametr(g, cc, params) 
    draw_graph(g, position(len(g.nodes)), params)
    plt.show()
    
    forest(cc, g, params)
    draw_graph(g, position(len(g.nodes)), params)
    plt.show()


if __name__ == '__main__':
    main()
